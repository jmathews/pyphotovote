# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Logs'
        db.create_table(u'photo_contest_logs', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=1024)),
        ))
        db.send_create_signal(u'photo_contest', ['Logs'])


    def backwards(self, orm):
        # Deleting model 'Logs'
        db.delete_table(u'photo_contest_logs')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'photo_contest.battle': {
            'Meta': {'object_name': 'Battle'},
            'competitors': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['photo_contest.Photo']", 'symmetrical': 'False'}),
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['photo_contest.Contest']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'voter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'winner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'victories'", 'null': 'True', 'to': u"orm['photo_contest.Photo']"})
        },
        u'photo_contest.contest': {
            'Meta': {'object_name': 'Contest'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_photos_per_user': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'maximum_file_size_MB': ('django.db.models.fields.IntegerField', [], {'default': '25'}),
            'minimum_shortest_dimension': ('django.db.models.fields.IntegerField', [], {'default': '1650'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'photos_per_battle': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'results_chart_count': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'results_histogram_step_size': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'subfolder': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'submission_end_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 30, 0, 0)'}),
            'submission_start_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 23, 0, 0)'}),
            'voting_end_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 1, 6, 0, 0)'}),
            'voting_start_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 30, 0, 0)'}),
            'winners_count': ('django.db.models.fields.IntegerField', [], {'default': '5'})
        },
        u'photo_contest.logs': {
            'Meta': {'object_name': 'Logs'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'photo_contest.photo': {
            'Meta': {'object_name': 'Photo'},
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['photo_contest.Contest']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo_file': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'photographer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'thumbnail_file_100': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'thumbnail_file_1024': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'thumbnail_file_200': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'thumbnail_file_300': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        }
    }

    complete_apps = ['photo_contest']