$(document).ready(function(){
    var scale = d3.scale.linear()
       .range([0, document.documentElement.clientHeight * 3/4]);

    d3.selectAll('.bar')
       .datum(function(){
                      return parseFloat(d3.select(this).attr('data-score'));
                  })
       .style('height', function(d){
                                     return scale(d) + 'px';
                                 });
    $('.bar').hover(
        function(e){
            img_url = $(this).data('url');
            hover_img = $('.hover_image_img');
            hover_img.attr('src', img_url);
            $('.hover_image').css({'visibility': 'visible', 'top': e.pageY, 'left': e.pageX});
            $('.image_text').html('score: ' + $(this).data('fraction') + '<br>' + $(this).data('name'));
        });
    $('.chart_container').hover(
        function(e){
            $('.hover_image').fadeIn();
        },
        function(e){
            $('.hover_image').fadeOut();
        }

    );

    $('.winning_image').each(function(){
        $(this).css(
            {'max-height': ($(this).height() * $(this).attr('data-score')) + 'px'}
        );
    });
});
